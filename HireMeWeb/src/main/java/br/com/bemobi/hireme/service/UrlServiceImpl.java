package br.com.bemobi.hireme.service;

import br.com.bemobi.hireme.dao.UrlDAOImpl;
import br.com.bemobi.hireme.domain.Url;

public class UrlServiceImpl implements UrlService {

	//TODO injetar depend�ncia
	UrlDAOImpl connect = new UrlDAOImpl();

	@Override
	public Url retrieve(Long id) {
		return connect.retrieve(id);
	}

	@Override
	public Url create(Url url) {
		return connect.create(url);
	}

}
