package br.com.bemobi.hireme.dto.converter;

import br.com.bemobi.hireme.domain.Url;
import br.com.bemobi.hireme.dto.UrlDTO;

public class UrlDTOConverter {

	public static UrlDTO convertToDTO(Url url){
		UrlDTO dto = new UrlDTO();
		
		dto.setId(url.getId());
		dto.setAlias(url.getAlias());
		dto.setUrl(url.getUrl());
		dto.setShortened(url.getShortened());
		
		return dto;
	}
	
	public static UrlDTO convertToDTO(Url url, long timeTaken) {
		UrlDTO dto = convertToDTO(url);
		dto.setTimeTaken(timeTaken);
		
		return dto;
	}
	
	public static Url convertToDomain(UrlDTO dto) {
		Url url = new Url();
		
		url.setId(dto.getId());
		url.setAlias(dto.getAlias());
		url.setUrl(dto.getUrl());
		url.setShortened(dto.getShortened());

		return url;
	}
	
}