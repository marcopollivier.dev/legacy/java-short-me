package br.com.bemobi.hireme.dao;

import br.com.bemobi.hireme.domain.Url;

public interface UrlDAO {

	public Url retrieve(Long id);
	public Url create(Url url);
	
}
