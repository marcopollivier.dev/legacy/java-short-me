package br.com.bemobi.hireme.service;

import br.com.bemobi.hireme.domain.Url;

public interface UrlService {

	public Url retrieve(Long id);
	public Url create(Url url);
	
}
