package br.com.bemobi.hireme.controller;

import java.sql.SQLException;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.bemobi.hireme.domain.Url;
import br.com.bemobi.hireme.dto.UrlDTO;
import br.com.bemobi.hireme.dto.converter.UrlDTOConverter;
import br.com.bemobi.hireme.service.UrlService;
import br.com.bemobi.hireme.service.UrlServiceImpl;

@Path("/url")
public class UrlControllerRS {

	//TODO injetar dependencia
	UrlService service = new UrlServiceImpl();
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Url getJSON(@PathParam("id") Long id) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return service.retrieve(id);
	}
	
	//TODO retrieve pelo alias 
	//TODO post usando parametros da URL (alias e url)
	//TODO retirar m�todo que insere por json
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public UrlDTO create(@QueryParam("url") String url) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {		
		System.out.println(url);
		return null;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public UrlDTO create(@QueryParam("url") String url, @QueryParam("alias") String alias) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {		
		System.out.println(url + alias);
		return null;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public UrlDTO create(Url url) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		//TODO pensar numa maneira elegante de trabalhar com esse intervalo de tempo		
		long start = new Date().getTime();
		Url response = service.create(url);
		long timetaken = new Date().getTime() - start;
		return UrlDTOConverter.convertToDTO(response, timetaken);
	}

}