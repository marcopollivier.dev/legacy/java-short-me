package br.com.bemobi.hireme.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.bemobi.hireme.domain.Url;

public class UrlDAOImpl implements UrlDAO{

//	@Inject
//	EntityManager entityManager;
//	
//	public Url retrieve(Long id) {
//		Url finded = entityManager.find(Url.class, id);
//		entityManager.close();
//		return finded;
//	}
//
//	public Url create(Url url) {
//		entityManager.getTransaction().begin();    
//		entityManager.persist(url);
//		entityManager.getTransaction().commit();  
//	
//		entityManager.close();
//		
//		return url;
//	}
	
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("hireme");
	
	public Url retrieve(Long id) {
		EntityManager manager = factory.createEntityManager();
		Url finded = manager.find(Url.class, id);
		manager.close();
		return finded;
	}

	public Url create(Url url) {
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();    
		manager.persist(url);
		manager.getTransaction().commit();  
	
		manager.close();
		
		return url;
	}

}